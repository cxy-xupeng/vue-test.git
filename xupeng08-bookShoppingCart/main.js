const app = new Vue({
	el:"#app",
	data:{
		books:[
			{
				id:1,
				name:'java',
				date:'2020-11',
				price:85.00,
				count:1
			},
			{
				id:2,
				name:'linux',
				date:'2020-11',
				price:59.00,
				count:1
			},
			{
				id:3,
				name:'python',
				date:'2020-11',
				price:39.00,
				count:1
			},
			{
				id:4,
				name:'java',
				date:'2020-11',
				price:128.00,
				count:1
			}
		]
	},
	methods:{
		/* getFinalPrice(price){
			return '￥'+price.toFixed(2)
		} */
		decrement(index){
			this.books[index].count--
		},
		increment(index){
			this.books[index].count++
		},
		removeHandle(index){
			this.books.splice(index,1)
		}
	},
	filters:{
		showPrice(price){
			return '￥'+price.toFixed(2)
		}
	},
	computed:{
		totalPrice(){
			let totalPrice =0
			/* 1.普通for */
			for(let i = 0;i<this.books.length;i++){
				totalPrice += this.books[i].count * this.books[i].price
			}
			
			/* 2.for in */
			for(let i in this.books){
				totalPrice += this.books[i].count * this.books[i].price
			}
			
			/* 3. for of */
			for(let book of this.books){
				totalPrice += book.count * book.price
			}
			
			return totalPrice
		}
	}
})