var name = '徐鹏'
var age = 18
var flag = true

function sum(a,b){
	return a+b;
}

if(flag){
	console.log(sum(100,200))
}

/* 1.导出方式一 */
export {
	flag,sum
}

/* 2.导出方式二 */
export var num1 = 1000
export var num2 = 1.88

/* 3.导出函数/类 */
export function mul(num1,num2){
	return num1 * num2
}
export class Person{
	run(){
		console.log('跑路')
	}
}

/* 4.导出default，每个js只能有一个default，引用的时候，可以随便命名 */
const address = '上海'
export default address

/* 5.统一全部导出 */
